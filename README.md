# Countdown Timer

## How To Run

Install dependencies:
```
> npm install
```

Run a webpack build:
```
> webpack
```

Start the dev server:
```
> npm run start
```

Open http://localhost:3000/ to view the project.


import React, {Component} from 'react';
import {connect} from 'react-redux';

class Timer extends Component {

    render() {
        if (!this.props.countDown) {
            return null;
        } else {
            return (
                <div>
                    <h2>{this.props.countDown}</h2>
                </div>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        countDown: state.countDown
    };
}

export default connect(mapStateToProps)(Timer);

import React, {Component} from 'react';
import {connect} from 'react-redux';

class ErrorMessage extends Component {

    render() {
        if (this.props.error) {
            return (
                <div className="error-message">
                    {this.props.error}
                </div>
            );
        } else {
            return null;
        }
    }
}

function mapStateToProps(state) {
    return {
        error: state.error
    };
}

export default connect(mapStateToProps)(ErrorMessage);

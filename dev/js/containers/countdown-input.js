import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {countDown} from '../actions/index'
import {displayError} from '../actions/index'

var timeLeft;

class CountdownInput extends Component {

    onClickStart() {
        this.props.displayError(null);
        clearInterval(timeLeft);
        this.props.countDown(null);

        let startTime = document.getElementById('startTime').value;
        let endTime = document.getElementById('endTime').value;

        if (!startTime) {
            this.props.displayError('The start time is invalid. Please enter a valid start time.');
        } else if (!endTime) {
            this.props.displayError('The end time is invalid. Please enter a valid end time.');
        } else if (startTime >= endTime) {
            this.props.displayError('Please enter an end time later than the start time.');
        } else {
            let totalTime = this.calculateTimeLeft(startTime, endTime);
            this.startCountdownTimer(totalTime);
        }
    }

    startCountdownTimer(totalTime) {
        this.props.countDown(totalTime);
        timeLeft = setInterval(() => {
            totalTime --;
            if (totalTime === 0) {
                clearInterval(timeLeft);
            }
            if (totalTime => 0) {
                this.props.countDown(totalTime);
            }
        }, 1000)
    }

    calculateTimeLeft(startTime, endTime) {
        return this.convertToSeconds(endTime) - this.convertToSeconds(startTime);
    }

    convertToSeconds(time) {
        let splitTime = time.split(':');
        let hourSeconds = splitTime[0] * 60 * 60;
        let minSeconds = splitTime[1] * 60;
        let secSeconds = splitTime[2];
        if (!secSeconds) {
            secSeconds = 0;
        }
        return Number(hourSeconds) + Number(minSeconds) + Number(secSeconds);
    }

    render() {
        return (
            <div className="margin-20">
                <div className="time-input margin-20">
                    <div className="input-label">Start Time</div>
                    <div>
                        <input id="startTime" type="time" step="1"></input>
                    </div>
                </div>
                <div className="time-input margin-20">
                    <div className="input-label">End Time</div>
                    <div>
                        <input id="endTime" type="time" step="1"></input>
                    </div>
                </div>
                <div>
                    <button onClick={() => this.onClickStart()}>
                        Start Countdown
                    </button>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        countDown: state.countDown
    };
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({countDown, displayError}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(CountdownInput);

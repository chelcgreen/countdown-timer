export const countDown = (timeLeft) => {
    return {
        type: 'COUNTDOWN',
        timeLeft: timeLeft,
    }
};

export const displayError = (errorMessage) => {
    return {
        type: 'DISPLAY_ERROR',
        error: errorMessage,
    }
};

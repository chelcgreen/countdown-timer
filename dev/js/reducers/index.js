import {combineReducers} from 'redux';
import Countdown from './reducer-countdown';
import Error from './reducer-error';

const allReducers = combineReducers({
    countDown: Countdown,
    error: Error,
});

export default allReducers

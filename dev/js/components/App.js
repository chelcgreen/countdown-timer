import React from 'react';
import CountdownInput from '../containers/countdown-input';
import Timer from '../containers/timer';
import ErrorMessage from '../containers/error-message';
require('../../css/style.css');

const App = () => (
    <div className="page-container">
        <ErrorMessage/>
        <CountdownInput/>
        <Timer/>
    </div>
);

export default App;
